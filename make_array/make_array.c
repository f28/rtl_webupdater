#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
	FILE* fd;
	ssize_t i, l;
	uint32_t value, index;
	uint8_t buffer;
	index = 0;
	if (argc !=2)
	{
		printf("usage: make_array input_file\n");
		exit(-1);
	}
	fd = fopen(argv[1], "rb");
	if (NULL == fd)
	{
		printf("Cannot open %s\n%s\n", argv[1], strerror(errno));
		exit(-1);
	}
	index=0;
	while(1)
	{
		l = fread((void*)&buffer, 1, 1, fd);
		if (l < 1)
			break;
		if (index!=0) {
			if ((index % 16)==0)
				printf(",\n");
			else printf(", ");
		}
		printf("0x%02X", (unsigned int)buffer);
		index++;
	}
	fclose(fd);
}

