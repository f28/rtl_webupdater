#=============================================
# SDK CONFIG
#=============================================
#USE_AT = 1
#USE_FATFS = 1
#USE_SDIOH = 1
#USE_POLARSSL = 1
#USE_P2P_WPS = 1
#USE_GCC_LIB = 1
USE_MBED = 1

ifndef USE_AT
USE_NEWCONSOLE = 1
USE_WIFI_API = 1
endif

#RTOSDIR=freertos_v8.1.2
RTOSDIR=freertos_v9.0.0
LWIPDIR=lwip_v1.4.1
include sdkset.mk
#compile
CFLAGS += -DLOGUART_STACK_SIZE=1024
#CFLAGS += -DDEFAULT_BAUDRATE=1562500
#=============================================
# User Files
#=============================================
# openocd freertos helper
ADD_SRC_C += project/src/FreeRTOS-openocd.c
#user main
ADD_SRC_C += project/src/user/main.c
ADD_SRC_C += project/src/user/http_server.c
ADD_SRC_C += project/src/user/flash_operation.c
ADD_SRC_C += project/src/user/cgiwifi_rtl.c

# my bootloader
BOOT_C += project/src/rtl_boot_s.c
#BOOT_C += $(LIBRTLHTTPD_PATH)../project/src/rtl_boot_s.c

ADD_SRC_C += $(THISDIR)$(BIN_DIR)/array_webfs.c
CFLAGS += -DESPFS_IN_RAM


INCLUDES += project/inc/user

# components
include $(LIBRTLHTTPD_PATH)librtlhttpd.mk

