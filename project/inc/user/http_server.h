/*
 * http_server.h
 *
 *  Created on: Feb 20, 2017
 *      Author: user003
 */

#ifndef HTTP_SERVER_H_
#define HTTP_SERVER_H_


//#define FLASH_APP_BASE  0xd0000
#define FLASH_APP_BASE  0xb8000



typedef struct cgiTp6State {
	int32_t start;
	int32_t end;
	int32_t pos;
} cgiTp6State_t;


void user_init_thrd(void);

void Reboot(void);

#endif /* HTTP_SERVER_H_ */
