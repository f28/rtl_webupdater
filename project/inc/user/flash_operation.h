/*
 * flash_operation.h
 *
 *  Created on: Apr 21, 2017
 *      Author: sharikov
 */

#ifndef FLASH_OPERATION_H_
#define FLASH_OPERATION_H_

// определение выодов для выбора прошивки
#define IMG_SELECT_PIN0 0x11  // PB.1
#define IMG_SELECT_PIN1 0xff  // в этом проекте не используется

void update_sysdata_sector(uint32_t newaddr);
uint32_t get_upgraded_image_addr(void);

#endif /* FLASH_OPERATION_H_ */
