#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "objects.h"
#include "flash_api.h"
#include "osdep_service.h"
#include "device_lock.h"
#include "semphr.h"
#include "wifi_constants.h"
#include "wifi_api.h"

#include "main.h"






#define ICACHE_FLASH_ATTR
// espfs
#include "espfsformat.h"
#include "espfs.h"
// librtlhttpd
#include "platform.h"

#include "flash_api.h"
#include "device_lock.h"
#include <platform/platform_stdlib.h>
#include "flash_eep.h"

#include "httpdespfs.h"
#include "soc_rtl8710_httpd_func.h"
//#include "netbios/netbios.h"
#include "captdns.h"

#include "http_server.h"
#include "flash_operation.h"
#include "cgiwifi_rtl.h"

#define SPI_FLASH_SEC_SIZE 4096
#define PAGELEN 256

typedef struct UploadState {
	uint32_t currentAddress;
	uint32_t len;
	uint32_t erasedAddress;
	uint32_t pageData[PAGELEN/4];  // aligned data buffer
	char *err;
	uint16_t pagePos;   // valid data count in pageData
	uint8_t  autoErase;
	uint8_t  state;
} UploadState_t;

typedef struct {
	//uint32_t start;  // start flash address
	uint32_t flash_size;

} CgiUploadFlashRtl_t;


extern uint32_t upgraded_image_addr;

#ifndef UPGRADE_FLAG_FINISH
#define UPGRADE_FLAG_FINISH     0x02
#endif

#define FLST_START 0
#define FLST_WRITE 1
#define FLST_DONE 2
#define FLST_ERROR 3


// MAC and WI-FI calibration data in flash
// must be sector aligned
#define CALIBRATION_START 0xa000
#define CALIBRATION_SIZE  SPI_FLASH_SEC_SIZE

#define EXCLUDE_AREA_START CALIBRATION_START
#define EXCLUDE_AREA_END  (CALIBRATION_START+CALIBRATION_SIZE)


extern int check_exclude_area(UploadState_t* state);


/* aligned / unaligned flash write with auto-erase
 */
extern void write_buffer(UploadState_t* state);



httpd_cgi_state cgiUpgradeImage(HttpdConnData *connData)
{
	CgiUploadFlashRtl_t *def=(CgiUploadFlashRtl_t*)connData->cgiArg;
	UploadState_t *state=(UploadState_t *)connData->cgiData;

	uint8_t *data;
	uint32_t dataLen;

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		if (state!=NULL) free(state);
		return HTTPD_CGI_DONE;
	}

	if (state==NULL) {
		//First call. Allocate and initialize state variable.
		dbg("Firmware upload cgi start.");

		if (upgraded_image_addr <= 0xb000)
		{
			httpd_error("Upgraded Image not found.");
			return HTTPD_CGI_DONE;
		}
		state=malloc(sizeof(UploadState_t));
		if (state==NULL) {
			httpd_error("Can't allocate firmware upload struct!");
			return HTTPD_CGI_DONE;
		}
		memset(state, 0, sizeof(UploadState_t));
		state->state=FLST_START;
		connData->cgiData=state;
		state->err="Premature end";
	}

	data=connData->post->buff;
	dataLen=connData->post->buffLen;

	while (dataLen!=0) {
		if (state->state==FLST_START) {
			dbg("POST Header length: %d", connData->priv->headPos);
			for (int d=0; d<= connData->priv->headPos; d++)
			{
				if (connData->priv->head[d]!=0x00)
					httpd_printf("%c", connData->priv->head[d]);
				//else
				//	DiagPrintf("\n");
			}
			uint8_t *b;
			b = malloc(20);

			//httpdGetHeader(connData, "x-start", b, 20);
			state->currentAddress = upgraded_image_addr; //atoi(b);
			//httpdGetHeader(connData, "x-erase", b, 20);
			state->autoErase = 1;
			httpdGetHeader(connData, "Content-Length", b, 20);
			state->len = atoi(b);
			/*dbg*/httpd_error("Start address=%d", state->currentAddress);
			/*dbg*/httpd_error("Length=%d\tErase=%d", state->len, state->autoErase);
			if (state->len ==0)
				return HTTPD_CGI_DONE;

			state->pagePos=0;
			state->erasedAddress = (state->currentAddress - SPI_FLASH_SEC_SIZE) &
					~(SPI_FLASH_SEC_SIZE-1);
			state->state=FLST_WRITE;
			free(b);
		} else if (state->state==FLST_WRITE) {
			int32_t lenLeft= PAGELEN - state->pagePos;   // free space in page buffer
			if (state->len < lenLeft) {
				dbg("Last buffer");
				lenLeft=state->len; //last buffer can be a cut-off one
			}
			//See if we need to write the page.
			if (dataLen<lenLeft) {
				//Page isn't done yet. Copy data to buffer and exit.
				memcpy(&state->pageData[state->pagePos], data, dataLen);
				state->pagePos+=dataLen;
				state->len-=dataLen;
				dataLen=0;
			}
			else {
				//Finish page; take data we need from post buffer
				memcpy(&state->pageData[state->pagePos], data, lenLeft);
				data+=lenLeft;
				dataLen-=lenLeft;
				state->pagePos+=lenLeft;
				state->len-=lenLeft;
				write_buffer(state);
			}

			if (state->len==0) {
				state->state=FLST_DONE;
				update_sysdata_sector(upgraded_image_addr);
			}

		} else if (state->state==FLST_DONE) {
			warn("Huh? %d bogus bytes received after data received.", dataLen);
			//Ignore those bytes.
			dataLen=0;
		} else if (state->state==FLST_ERROR) {
			//Just eat up any bytes we receive.
			httpd_error("FLST_ERROR");
			dataLen=0;
		}
	}

	if (connData->post->len==connData->post->received) {
		//We're done! Format a response.
		info("Upload done. Sending response.");
		httpdStartResponse(connData, state->state==FLST_ERROR?400:200);
		httpdHeader(connData, "Content-Type", "text/plain");
		httpdEndHeaders(connData);
		if (state->state!=FLST_DONE) {
			httpdSend(connData, "Firmware image error:", -1);
			httpdSend(connData, state->err, -1);
			httpdSend(connData, "\n", -1);
		}
		free(state);
		return HTTPD_CGI_DONE;
	}

	return HTTPD_CGI_MORE;
}


httpd_cgi_state cgiResetWiFi(HttpdConnData *connData) {
	uint32_t count, j;
	uint8_t *buff;
	uint8_t e=0;
	uint32_t faddr=FMEMORY_SCFG_BASE_ADDR;

	httpdStartResponse(connData, 200);
	httpdHeader(connData, "Content-Type", "text/plain");
	httpdEndHeaders(connData);

	buff = malloc(FMEMORY_SCFG_BANK_SIZE);
	if (!buff) {
		httpdSend(connData, "Error!", -1);
		return HTTPD_CGI_DONE;
	}

	device_mutex_lock(RT_DEV_LOCK_FLASH);

	for (count = 0; count < FMEMORY_SCFG_BANKS; count++) {
		flash_stream_read(&flashobj, faddr, FMEMORY_SCFG_BANK_SIZE,
				buff);
		for (j=0; j<FMEMORY_SCFG_BANK_SIZE; j++) {
			if (buff[j] != 0xff) {
				e=1;
				flash_erase_sector(&flashobj, faddr);
				rtl_printf("erase addr=0x%x\n", faddr);
				break;
			}
		}
		faddr+=FMEMORY_SCFG_BANK_SIZE;
	}
	device_mutex_unlock(RT_DEV_LOCK_FLASH);
	free(buff);
	if (e)
		httpdSend(connData, "WiFi Settings erased.", -1);
	else
		httpdSend(connData, "WiFi Settings is clean.", -1);
	return HTTPD_CGI_DONE;
}


extern int cgiRebootFirmware(HttpdConnData *connData);

CgiUploadFlashRtl_t uploadParams = {
		.flash_size=1048576,

};
HttpdBuiltInUrl builtInUrls[]=
{
		ROUTE_REDIRECT("/", "/index.html"),

		ROUTE_CGI_ARG("/upload", cgiUpgradeImage, &uploadParams),
		ROUTE_CGI("/resetwifi", cgiResetWiFi),
		ROUTE_CGI("/reboot", cgiRebootFirmware),

		ROUTE_REDIRECT("/wifi", "/wifi/setup_sta.tpl"),
		ROUTE_REDIRECT("/wifi/", "/wifi/setup_sta.tpl"),
		ROUTE_TPL_FILE("/wifi/setup_sta.tpl", tplWlan, "/wifi/setup_sta.tpl.html"),
		ROUTE_CGI("/wifi/wifiscan.cgi", cgiWiFiScan),
		ROUTE_CGI("/wifi/connect.cgi", cgiWiFiConnect),
		ROUTE_FILE("*", NULL),    //Catch-all cgi function for the filesystem
		{NULL, NULL, NULL}
};



extern char array_webfs[];
void user_start(void)
{
	EspFsInitResult e = ESPFS_INIT_RESULT_NO_IMAGE;

	captdnsInit();

	upgraded_image_addr = get_upgraded_image_addr();
	update_sysdata_sector(0);

	vTaskDelay(50);

	e=espFsInit((void*)array_webfs);

	httpdInit(builtInUrls, 80);

	if (e)
		rtl_printf("Espfs not found.\n");

	rtl_printf("[After httpdInit]: RAM heap\t%d bytes\tTCM heap\t%d bytes\n",
			xPortGetFreeHeapSize(), tcm_heap_freeSpace());
}



void user_init_thrd(void) {
	/* Initilaize the console stack */
	console_init();

	wifi_init();

	user_start();

	/* Kill init thread after all init tasks done */
	vTaskDelete(NULL);
}


extern unsigned char wifi_run_mode;
extern unsigned char wifi_st_status;
void Reboot(void) {
	rtl_printf("Resetting...");

	httpdStop(255);
	//wifi_run(RTW_MODE_NONE);
	if(wifi_run_mode) {
		wifi_disconnect();
	};
	wifi_off();
	wifi_st_status = WIFI_STA_OFF;
	wifi_run_mode = RTW_MODE_NONE;

	vTaskDelay(100);

	taskENTER_CRITICAL();
	// boot from flash
	HAL_WRITE32(SYSTEM_CTRL_BASE, 0x210, 0x211157); 	//mww 0x40000210 0x211157

	// Set processor clock to default before system reset
	HAL_WRITE32(SYSTEM_CTRL_BASE, 0x14, 0x00000021);
	HalDelayUs(50000);
	// Cortex-M3 SCB->AIRCR
	HAL_WRITE32(0xE000ED00, 0x0C, (0x5FA << 16) |                             // VECTKEY
			(HAL_READ32(0xE000ED00, 0x0C) & (7 << 8)) | // PRIGROUP
			(1 << 2));                                  // SYSRESETREQ
	while(1)  asm volatile ("nop\n\t");
}
