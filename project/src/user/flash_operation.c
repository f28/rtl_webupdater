#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "objects.h"
#include "flash_api.h"

#include <stdint.h>
#include <string.h>
//#include "platform.h"

#include "flash_api.h"
#include "device_lock.h"
#include <platform/platform_stdlib.h>
#include "flash_operation.h"

uint32_t upgraded_image_addr;

typedef struct _seg_header {
	uint32 size;
	uint32 ldaddr;
} IMGSEGHEAD, *PIMGSEGHEAD;
typedef struct _img2_header {
	IMGSEGHEAD seg;
	uint32 sign[2];
	void (*startfunc)(void);
	uint8 rtkwin[7];
	uint8 ver[13];
	uint8 name[32];
} IMG2HEAD, *PIMG2HEAD;

enum {
	SEG_ID2_ERR,
	SEG_ID2_SRAM,
	SEG_ID2_TCM,
	SEG_ID2_SDRAM,
	SEG_ID2_SOC,
	SEG_ID2_FLASH,
	SEG_ID2_CPU,
	SEG_ID2_ROM,
	SEG_ID2_MAX
} SEG_ID2;

static const char * const txt_tab_seg[] = {
		"UNK",		// 0
		"SRAM",		// 1
		"TCM",		// 2
		"SDRAM",	// 3
		"SOC",		// 4
		"FLASH",	// 5
		"CPU",		// 6
		"ROM"		// 7
};

static const uint32 tab_seg_def[] = { 0x10000000, 0x10070000, 0x1fff0000,
		0x20000000, 0x30000000, 0x30200000, 0x40000000, 0x40800000, 0x98000000,
		0xA0000000, 0xE0000000, 0xE0010000, 0x00000000, 0x00050000 };

static uint32_t get_seg_id(uint32 addr, int32 size) {
	uint32 ret = SEG_ID2_ERR;
	uint32 * ptr = &tab_seg_def;
	if (size > 0) {
		do {
			ret++;
			if (addr >= ptr[0] && addr + size <= ptr[1]) {
				return ret;
			};
			ptr += 2;
		} while (ret < SEG_ID2_MAX);
	};
	return SEG_ID2_ERR;
}

static uint32 load_img2_head(uint32 faddr, PIMG2HEAD hdr) {
	//flashcpy(faddr, hdr, sizeof(IMG2HEAD));
	device_mutex_lock(RT_DEV_LOCK_FLASH);
	flash_stream_read(&flashobj, faddr, sizeof(IMG2HEAD), (uint8_t *)hdr);
	device_mutex_unlock(RT_DEV_LOCK_FLASH);

	uint32 ret = get_seg_id(hdr->seg.ldaddr, hdr->seg.size);
	if (hdr->sign[1] == IMG_SIGN2_RUN) {
		if (hdr->sign[0] == IMG_SIGN1_RUN) {
			ret |= 1 << 9;
		} else if (hdr->sign[0] == IMG_SIGN1_SWP) {
			ret |= 1 << 8;
		};
	}
	if (*(u32 *) (&hdr->rtkwin) == IMG2_SIGN_DW1_TXT) {
		ret |= 1 << 10;
	};
	return ret;
}


/* стирает и записывает значения по умолчанию в сектор System Data (0x9000)
 *
 */
void update_sysdata_sector(uint32_t newaddr) {
	uint8_t update=0;
	uint32_t img2addr;
	uint8_t  buff[8+4];
	uint8_t *gpio_pin = &buff[8];

	memset(buff, 255, sizeof(buff));

	device_mutex_lock(RT_DEV_LOCK_FLASH);

	// адрес upgraded Image
	flash_stream_read(&flashobj, FLASH_SYSTEM_DATA_ADDR, sizeof(img2addr), (uint8_t*)&img2addr);
	// выводы для выбора прошивки
	flash_stream_read(&flashobj, FLASH_SYSTEM_DATA_ADDR+0x08, 4, gpio_pin);

	memcpy(buff, &img2addr, 4);
	if ((img2addr != newaddr) && (newaddr > 0xb000)) {
		memcpy(buff, &newaddr, 4);
		update=1;
	}
	if ((gpio_pin[0] !=IMG_SELECT_PIN0) || (gpio_pin[1] !=IMG_SELECT_PIN1)) {
		gpio_pin[0] =IMG_SELECT_PIN0;
		gpio_pin[1] =IMG_SELECT_PIN1;
		gpio_pin[2] = 0xff;
		gpio_pin[3] = 0xff;
		update=1;
	}
	if (update) {
		// стираем сектор FLASH_SYSTEM_DATA_ADDR
		flash_erase_sector(&flashobj, FLASH_SYSTEM_DATA_ADDR);
		// записываем адрес upgraded Image и выводы выбора прошивки
		flash_burst_write(&flashobj, FLASH_SYSTEM_DATA_ADDR,
				sizeof(buff), buff);
	}
	device_mutex_unlock(RT_DEV_LOCK_FLASH);
}

int32_t skip_seqs(int32_t faddr, PIMG2HEAD hdr) {
	int32_t fnextaddr = faddr;
	uint8_t segnum = 0;
	while (1) {
		uint32_t seg_id = get_seg_id(hdr->seg.ldaddr, hdr->seg.size);
		//rtl_printf("fnextaddr=0x%x  seg_id=0x%x\n", fnextaddr, seg_id);
		if (seg_id) {
#if CONFIG_DEBUG_LOG > 2
			rtl_printf("Skip Flash seg%d: 0x%08x -> %s: 0x%08x, size: %d\n", segnum,
					faddr, txt_tab_seg[seg_id], hdr->seg.ldaddr, hdr->seg.size);
#endif
			fnextaddr += hdr->seg.size;
		} else {
			break;
		}
		//fnextaddr += flashcpy(fnextaddr, &hdr->seg, sizeof(IMGSEGHEAD));
		device_mutex_lock(RT_DEV_LOCK_FLASH);
		flash_stream_read(&flashobj, fnextaddr, sizeof(IMGSEGHEAD), (uint8_t *)&hdr->seg);
		device_mutex_unlock(RT_DEV_LOCK_FLASH);
		fnextaddr += sizeof(IMGSEGHEAD);
		segnum++;
	}
	return fnextaddr;
}
/* возвращает адрес второй прошивки
 * это следующий сектор после конца нулевой прошивки
 * 0xb000 если нулевой прошивки нет
 */
uint32_t get_upgraded_image_addr(void) {
	IMG2HEAD hdr;
	uint32_t faddr=0xb000;
	uint8_t imgnum=0;
	while (imgnum ==0) {
		faddr = (faddr + FLASH_SECTOR_SIZE - 1) & (~(FLASH_SECTOR_SIZE - 1));
		// получим тип первого сегмента прошивки
		uint32_t img_id = load_img2_head(faddr, &hdr);
		// заголовок валиден ?
		if ((img_id >> 8) > 4 || (uint8_t) img_id != 0) {
			imgnum++;
			// парсим заголовки всех сегментов этой прошивки
			faddr = skip_seqs(faddr + 0x10, &hdr.seg);
		}
		else {
			if (imgnum == 0) {
				rtl_printf("No Image!\n");
				return 0xb000;
			}
			break;
		}
	}
	faddr =  (faddr + FLASH_SECTOR_SIZE - 1) & (~(FLASH_SECTOR_SIZE - 1));
	rtl_printf("Upgraded Image address=0x%x\n", faddr);
	return faddr;
}
