/*
Cgi/template routines for the /wifi url.
 */

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Jeroen Domburg <jeroen@spritesmods.com> wrote this file. As long as you retain 
 * this notice you can do whatever you want with this stuff. If we meet some day, 
 * and you think this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */

#include "user_config.h"

#include "autoconf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "lwip/tcp.h"
#include "flash_eep.h"
#include "device_lock.h"
#include "wifi_api.h"
#include <lwip_netconf.h>



#include "platform.h"
#include "soc_rtl8710_httpd_func.h"
#include "cgiwifi_rtl.h"
//#include "sys_cfg.h"
#include "esp_comp.h"




#define CONNTRY_IDLE 0
#define CONNTRY_WORKING 1
#define CONNTRY_SUCCESS 2
#define CONNTRY_FAIL 3

//Connection result var
//static int connTryStatus=CONNTRY_IDLE;
//Temp store for new ap info.
//static rtw_network_info_t stconf;

static volatile uint8_t setap_and_restart =0;
//static volatile uint8_t wifi_st_connect_protect =0;

extern unsigned char wifi_run_mode;
extern struct netif xnetif[NET_IF_NUM]; /* network interface structure */



httpd_cgi_state tplSetupAp(HttpdConnData *connData, char *token, void **arg) {
	char *buff;
	rtw_wifi_setting_t wconf;
	int l=0;

	if (token==NULL) return HTTPD_CGI_DONE;

	buff=rtw_zmalloc(1024);
	if (!buff)
		return HTTPD_CGI_DONE;

	if (wifi_mode==RTW_MODE_AP)
		wifi_get_setting(WLAN0_NAME, &wconf);
	else
		wifi_get_setting(WLAN1_NAME, &wconf);

	if (strcmp(token, "wifi_cmode")==0) {
		l+=sprintf(&buff[l], "%d", wifi_cfg.mode);
	}
	else if (strcmp(token, "wifi_ap_chl")==0) {
		l+=sprintf(&buff[l], "%u", wifi_ap_cfg.channel);
	}
	else if (strcmp(token, "wifi_bgn")==0) {
		l+=sprintf(&buff[l], "%d", wifi_cfg.bgn);
	}
	else if (strcmp(token, "wifi_txpow")==0) {
		l+=sprintf(&buff[l], "%u", wifi_cfg.tx_pwr);
	}
	else if (strcmp(token, "wifi_country")==0) {
		l+=sprintf(&buff[l], "%u", wifi_cfg.country_code);
	}

	else if (strcmp(token, "wifi_ap_mac")==0) {
		l+=sprintf(&buff[l], MACSTR, MAC2STR(xnetif[WLAN_AP_NETIF_NUM].hwaddr));
	}

	else if (strcmp(token, "wifi_ap_ssid")==0) {
		wifi_ap_cfg.ssid[NDIS_802_11_LENGTH_SSID] = '\0';
		l+=sprintf(&buff[l], "%s",wifi_ap_cfg.ssid);
	}
	else if (strcmp(token, "wifi_ap_psw")==0) {
		wifi_ap_cfg.password[IW_PASSPHRASE_MAX_SIZE] = '\0';
		l+=sprintf(&buff[l], "%s", wifi_ap_cfg.password);
	}
	else if (strcmp(token, "wifi_ap_auth")==0) {
		l+=sprintf(&buff[l], "%c", (wifi_ap_cfg.security) ? '1' : '0');
	}
	else if (strcmp(token, "wifi_ap_ip")==0) {
		l+=sprintf(&buff[l], IPSTR, IP2STR(&wifi_ap_dhcp.ip));
	}
	else if (strcmp(token, "wifi_ap_msk")==0) {
		l+=sprintf(&buff[l], IPSTR, IP2STR(&wifi_ap_dhcp.mask));
	}
	else if (strcmp(token, "wifi_ap_gw")==0) {
		l+=sprintf(&buff[l], IPSTR, IP2STR(&wifi_ap_dhcp.gw));
	}
	else if (strcmp(token, "wifi_ap_dhcp")==0) {
		l+=sprintf(&buff[l], "%c", (wifi_ap_dhcp.mode)? '1' : '0');
	}
	/*
	else if (strcmp(token, "wifi_ap_cdns")==0) {
		l+=sprintf(&buff[l], "%c", (syscfg.cfg & (1<<SYSCFG_CDNS))? '1' : '0');	}
		*/
	httpdSend(connData, buff, -1);
	vPortFree(buff);
	return HTTPD_CGI_DONE;
}

extern unsigned char wifi_run_mode;
extern unsigned char wifi_st_status;
void setApStRTask(void) {
	if (setap_and_restart==1) {
		//wifi_cfg.save_flg = BID_WIFI_AP_CFG | BID_AP_DHCP_CFG | BID_WIFI_CFG; //DEF_SAVE_CFG;
		/*
		write_wifi_cfg(BID_WIFI_AP_CFG | BID_AP_DHCP_CFG | BID_WIFI_CFG);
		flash_write_cfg(&syscfg, FEEP_ID_SYS_CFG, sizeof(syscfg));
		*/
		Reboot();
	}
	else {
		//write_wifi_cfg(BID_WIFI_ST_CFG | BID_ST_DHCP_CFG);
		if(wifi_run_mode) {
			wifi_disconnect();
		};
		wifi_off();
		wifi_st_status = WIFI_STA_OFF;
		wifi_run_mode = RTW_MODE_NONE;
		//LwIP_DHCP(0, DHCP_STOP);
		vTaskDelay(250);
		wifi_run(wifi_cfg.mode);

		vTaskDelay(250);
		//httpd_start();
	}
	setap_and_restart =0;
	vTaskDelete(NULL);
}


httpd_cgi_state cgiWiFiSetAp(HttpdConnData *connData) {
	char *buff;
	int32_t  len;
	int32_t code=200;
	uint32_t utmp;

	if (connData->conn==NULL)
		return HTTPD_CGI_DONE;  //Connection aborted. Clean up.

	buff=rtw_zmalloc(1024);
	if (buff ==NULL)
		return HTTPD_CGI_DONE;

	rtl_printf("%s\n", connData->post->buff);

	len = httpdFindArg(connData->post->buff, "wifi_mode", buff, 8);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_cfg.mode = utmp;
		//rtl_printf("wifi_mode=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_phy", buff, 8);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_cfg.bgn=utmp;
		//rtl_printf("wifi_phy=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_ap_chl", buff, 8);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_ap_cfg.channel=utmp;
		//rtl_printf("wifi_ap_chl=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_txpow", buff, 8);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_cfg.tx_pwr=utmp;
		//rtl_printf("wifi_txpow=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_country", buff, 8);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_cfg.country_code=utmp;
		//rtl_printf("wifi_country=%u\n", utmp);
	}

	len = httpdFindArg(connData->post->buff, "wifi_ap_ssid", buff, NDIS_802_11_LENGTH_SSID+1);
	if (len >0) {
		buff[NDIS_802_11_LENGTH_SSID] = '\0';
		sprintf(wifi_ap_cfg.ssid, "%s", buff);
		//rtl_printf("wifi_ap_ssid: %s\n", buff);
	}
	len = httpdFindArg(connData->post->buff, "wifi_ap_psw", buff, IW_PASSPHRASE_MAX_SIZE+1);
	if (len >0) {
		buff[IW_PASSPHRASE_MAX_SIZE] = '\0';
		sprintf(wifi_ap_cfg.password, "%s", buff);
		//rtl_printf("wifi_ap_psw: %s\n", buff);
	}

	len = httpdFindArg(connData->post->buff, "wifi_ap_auth", buff, 20);
	if (len >0) {
		utmp=strtoul(buff, NULL, 0);
		wifi_ap_cfg.security =utmp;
		rtl_printf("wifi_ap_auth=%u\n", utmp);
	}
	memset(buff, 0, 21);
	len = httpdFindArg(connData->post->buff, "wifi_ap_ip", buff, 20);
	if (len >0) {
		wifi_ap_dhcp.ip=ipaddr_addr(buff);
		//rtl_printf("wifi_ap_ip=%s\n", buff);
	}
	memset(buff, 0, 21);
	len = httpdFindArg(connData->post->buff, "wifi_ap_msk", buff, 20);
	if (len >0) {
		wifi_ap_dhcp.mask=ipaddr_addr(buff);
		//rtl_printf("wifi_ap_msk=%s\n", buff);
	}
	memset(buff, 0, 21);
	len = httpdFindArg(connData->post->buff, "wifi_ap_gw", buff, 20);
	if (len >0) {
		wifi_ap_dhcp.gw=ipaddr_addr(buff);
		//rtl_printf("wifi_ap_gw=%s\n", buff);
	}
	len = httpdFindArg(connData->post->buff, "wifi_ap_dhcp", buff, 8);
	utmp=0;
	if (len >0)
		utmp=1;

	wifi_ap_dhcp.mode = (utmp)? 2 :0;
	rtl_printf("wifi_ap_dhcp=%u\n", utmp);

	/*
	len = httpdFindArg(connData->post->buff, "wifi_ap_cdns", buff, 8);
	if (len >0) {
		syscfg.cfg |= (1<<SYSCFG_CDNS);
		//rtl_printf("wifi_ap_cdns=1\n");
	}
	else {
		syscfg.cfg &= ~(1<<SYSCFG_CDNS);
		//rtl_printf("wifi_ap_cdns=0\n");
	}
	*/

	vPortFree(buff);

	if (setap_and_restart ==0) {
		setap_and_restart = 1;
		xTaskCreate(setApStRTask, "setApRTask", 384, NULL, tskIDLE_PRIORITY + 0 + PRIORITIE_OFFSET, NULL);
	}
	else
		code = 503;

	httpdStartResponse(connData, code);
	httpdHeader(connData, "Content-Type", "text/plain");
	httpdEndHeaders(connData);


	httpdSend(connData, "Rebooting...", -1);

	return HTTPD_CGI_DONE;
}


extern void (*p_wlan_autoreconnect_hdl)(rtw_security_t, char*, int, char*, int,	int);
//This cgi uses the routines above to connect to a specific access point with the
//given ESSID using the given password.
httpd_cgi_state cgiWiFiConnect(HttpdConnData *connData) {
	int32_t code=200;
	const char* http_msg=NULL;
	char *essid;
	char *passwd;
	char *enc_s;
	int essid_len, passwd_len, len;
	uint32_t utmp;

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	if (setap_and_restart !=0) {
		code = 500;
		http_msg = "Server error: busy!\r\n";
		goto cgiWiFiConnect_exit;
	}


	essid=rtw_zmalloc(128);
	passwd=rtw_zmalloc(128);
	if ((essid ==NULL) || (passwd == NULL)) {
		code = 500;
		http_msg = "Server error: malloc failed!\r\n";
		goto cgiWiFiConnect_exit;
	}

	len = httpdFindArg(connData->post->buff, "wifi_st_ip", essid, 128);
	if (len > 0) {
		utmp=ipaddr_addr(essid);
		wifi_st_dhcp.ip = utmp;
		rtl_printf("wifi_st_ip=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_ap_msk", essid, 128);
	if (len > 0) {
		utmp=ipaddr_addr(essid);
		wifi_st_dhcp.mask = utmp;
		rtl_printf("wifi_ap_msk=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_st_gw", essid, 128);
	if (len > 0) {
		utmp=ipaddr_addr(essid);
		wifi_st_dhcp.gw = utmp;
		rtl_printf("wifi_st_gw=%u\n", utmp);
	}

	len = httpdFindArg(connData->post->buff, "wifi_st_dhcp", essid, 128);
	if (len > 0) {
		utmp=strtoul(essid, NULL, 0);
		if (utmp > 3) {
			code=400;
			http_msg = "Bad request [wifi_st_dhcp]\r\n";
			goto cgiWiFiConnect_exit;
		}
		wifi_st_dhcp.mode = utmp;
		rtl_printf("wifi_st_dhcp=%u\n", utmp);
	}

	len = httpdFindArg(connData->post->buff, "wifi_st_arec", essid, 128);
	if (len > 0) {
		utmp=strtoul(essid, NULL, 0);
		if (utmp > 255) {
			code=400;
			http_msg = "Bad request [wifi_st_arec]\r\n";
			goto cgiWiFiConnect_exit;

		}
		wifi_st_cfg.autoreconnect = (utmp < 256)? utmp : 255;
		rtl_printf("wifi_st_arec=%u\n", utmp);
	}
	len = httpdFindArg(connData->post->buff, "wifi_st_rect", essid, 128);
	if (len > 0) {
		utmp=strtoul(essid, NULL, 0);
		if (utmp > 255) {
			code=400;
			http_msg = "Bad request [wifi_st_rect]\r\n";
			goto cgiWiFiConnect_exit;

		}
		wifi_st_cfg.reconnect_pause = (utmp < 256)? utmp : 255;
		rtl_printf("wifi_st_rect=%u\n", utmp);
	}


	len = httpdFindArg(connData->post->buff, "enc", essid, 128);
	if (len <=0)
		wifi_st_cfg.security = 0;
	else {
		essid[len]=0x00;
		wifi_st_cfg.security = strtol(essid, NULL, 10) & 255;
	}

	memset(essid, 0 , 128);
	essid_len = httpdFindArg(connData->post->buff, "essid", essid, 128);
	if (essid_len <=0) {
		code=400;
		http_msg = "Bad request [essid]]!\r\n";
		goto cgiWiFiConnect_exit;
	}
	if (essid_len >NDIS_802_11_LENGTH_SSID)
		essid_len = NDIS_802_11_LENGTH_SSID;
	essid[essid_len]=0x00;

	passwd_len = httpdFindArg(connData->post->buff, "passwd", passwd, 128);
	if (passwd_len <=0)
		goto cgiWiFiConnect_exit;
	if (passwd_len >IW_PASSPHRASE_MAX_SIZE)
		passwd_len = IW_PASSPHRASE_MAX_SIZE;
	passwd[passwd_len]=0x00;


	strncpy((char*)wifi_st_cfg.ssid, essid, essid_len);
	wifi_st_cfg.ssid[essid_len]=0x00;
	strncpy((char*)wifi_st_cfg.password, passwd, passwd_len);
	wifi_st_cfg.password[passwd_len]=0x00;

	rtl_printf("ssid=%s\n", wifi_st_cfg.ssid);
	rtl_printf("pass=%s\n", wifi_st_cfg.password);

	httpdRedirect(connData, "/wifi");
	code = 302;

#if 1
	if ((wifi_run_mode == RTW_MODE_STA) || (wifi_run_mode == RTW_MODE_STA_AP)) {
		if(wifi_mode) {
			int ret;
			info_printf("Deinitializing WIFI ...\n");
			//st_set_autoreconnect(0, 0, wifi_st_cfg.reconnect_pause);
			ret = wext_set_autoreconnect(WLAN0_NAME, 0, wifi_st_cfg.autoreconnect, wifi_st_cfg.reconnect_pause);
			if (ret != RTW_SUCCESS)
				warning_printf("ERROR: Operation failed! Error=%d\n", ret);

			vTaskDelay(30);
		}

	}
#endif

	cgiWiFiConnect_exit:

	if (passwd !=NULL)
		vPortFree(passwd);
	if (essid !=NULL)
		vPortFree(essid);

	if (code == 302) {
		if (setap_and_restart ==0) {
			setap_and_restart = 2;
			xTaskCreate(setApStRTask, "setApRTask", 384, NULL, tskIDLE_PRIORITY + 0 + PRIORITIE_OFFSET, NULL);
			http_msg = NULL;
		}
		else {
			code = 503;
			http_msg = "Error!";
		}
	}
	if (code !=302) {
		httpdStartResponse(connData, code);
		httpdEndHeaders(connData);
		if (http_msg != NULL)
			httpdSend(connData, http_msg, -1);
	}

	return HTTPD_CGI_DONE;
}






uint16_t jsonCodeEscape(uint8_t *d, uint8_t *s, uint16_t lens)
{
	uint16_t ret = 0;
	if (s != NULL) {

		while ((lens--) && (*s != 0x00)) {
			switch (*s) {
			case 0x5c:  // '\'
				*d++ = 0x5c;
				*d++ = *s++;
				ret +=2;
				break;
			case '"':
				*d++ = 0x5c;
				*d++ = *s++;
				ret +=2;
			default:
				*d++ = *s++;
				ret ++;
			} // switch
		}  // while
	}

	*d = 0x00;
	return ret;
}

//This CGI is called from the bit of AJAX-code in wifi.tpl. It will initiate a
//scan for access points and if available will return the result of an earlier scan.
//The result is embedded in a bit of JSON parsed by the javascript in wifi.tpl.
httpd_cgi_state cgiWiFiScan(HttpdConnData *connData) {
	char *buff;
	int len;
	cgiWifiScanData_t * wifiscan_data=(cgiWifiScanData_t*)connData->cgiData;
	web_scan_handler_t * pwscn_rec = &web_scan_handler_ptr;

	buff=rtw_zmalloc(1024);
	if (buff == NULL) {
		httpdStartResponse(connData, 500);
		httpdEndHeaders(connData);
		httpdSend(connData, "Server error: malloc failed!", -1);
		return HTTPD_CGI_DONE;
	}

	if (wifiscan_data == NULL) {
		wifiscan_data = rtw_zmalloc(sizeof(cgiWifiScanData_t));
		if (wifiscan_data == NULL) {
			httpdStartResponse(connData, 500);
			httpdEndHeaders(connData);
			httpdSend(connData, "Server error: malloc failed!", -1);
			goto wifi_scan_exit;
		}

		connData->cgiData = wifiscan_data;
		httpdStartResponse(connData, 200);
		httpdHeader(connData, "Content-Type", "text/json");
		httpdEndHeaders(connData);

		if (pwscn_rec->flg ==0) {
			api_wifi_scan(NULL);   // start new scan
			wifi_set_timer_scan(10000);
		}
	}

	if (pwscn_rec->flg ==1) {
		//We're still scanning. Tell Javascript code that.
		len=sprintf(buff, "{\n \"result\": { \n\"inProgress\":1\n }\n}\n");
		httpdSend(connData, buff, len);
		vPortFree(buff);
		return HTTPD_CGI_DONE;
	}
	else {
		//Scan completed. Send AP list
		if (wifiscan_data->ap_count ==0) {
			len=sprintf(buff, "{\n \"result\": { \n\"inProgress\":0, \n\"APs\": [\n");
			httpdSend(connData, buff, len);
		}
		while (wifiscan_data->ap_count < pwscn_rec->ap_count) {
			rtw_scan_result_t *scanresult = &pwscn_rec->ap_details[wifiscan_data->ap_count];
			uint8_t ssid[32*2 + 1];

			len = scanresult->SSID.len;
			if (len > 32) len = 32;
			jsonCodeEscape(ssid, &scanresult->SSID.val, len);

			len = sprintf(buff, "{\"essid\": \"%s\", \"bssid\": \"" MAC_FMT "\", \"rssi\": %d, \"enc\": %d, \"channel\": %d }%s\n",
					ssid, MAC_ARG(scanresult->BSSID.octet),
					scanresult->signal_strength,
					rtw_security_to_idx(scanresult->security),
					scanresult->channel,
					(wifiscan_data->ap_count == pwscn_rec->ap_count-1)? "":",");

			if (len > httpGetSendBuffLength(connData)-6) {
				vPortFree(buff);
				return HTTPD_CGI_MORE;
			}

			//buff[len]=0x00;
			//rtl_printf("%s", buff);

			httpdSend(connData, buff, len);
			wifiscan_data->ap_count++;
		}
		len=sprintf(buff, "]\n}\n}\n");
		httpdSend(connData, buff, len);
	}

	wifi_scan_exit:

	if (wifiscan_data !=NULL)
		vPortFree(wifiscan_data);

	vPortFree(buff);
	return HTTPD_CGI_DONE;
}

//Template code for the WLAN page.
httpd_cgi_state tplWlan(HttpdConnData *connData, char *token, void **arg) {
	char *buff;
	rtw_wifi_setting_t stconf;
	const char* wifi_st2string[]={
			"OFF",
			"START",
			"RECONNECT",
			"CONNECTED"
	};
	const char* wifi_mode2string[] = {
			"NONE",
			"STATION",
			"SOFT AP",
			"STATION + AP",
			"PROMISC",
			"P2P"
	};
	if (token==NULL) return HTTPD_CGI_DONE;




	buff=rtw_zmalloc(1024);
	if (!buff)
		return HTTPD_CGI_DONE;

	wifi_get_setting(WLAN0_NAME, &stconf);

	strcpy(buff, "Unknown");
	if (strcmp(token, "WiFiMode")==0) {
		if (wifi_mode <= RTW_MODE_P2P )
			strcpy(buff, wifi_mode2string[wifi_mode]);
	}
	else if (strcmp(token, "StaMAC")==0) {
		sprintf(buff, MACSTR, MAC2STR(xnetif[WLAN_ST_NETIF_NUM].hwaddr));
	} else if (strcmp(token, "wifi_st_status")==0) {
		sprintf(buff, "%s", wifi_st2string[wifi_st_status & 3]);
	} else if (strcmp(token, "currRssi")==0) {
		int st_rssi;
		//wext_get_rssi(const char *ifname, int *rssi)
		wifi_get_rssi(&st_rssi);
		sprintf(buff, "%d", st_rssi);
	} else if (strcmp(token, "currSsid")==0) {
		strcpy(buff, (char*)stconf.ssid);
	} else if (strcmp(token, "WiFiPasswd")==0) {
		strcpy(buff, (char*)stconf.password);
	} else if (strcmp(token, "wifi_st_ip")==0) {
		sprintf(buff, IPSTR, IP2STR(&xnetif[WLAN_ST_NETIF_NUM].ip_addr));
	} else if (strcmp(token, "wifi_st_msk")==0) {
		sprintf(buff, IPSTR, IP2STR(&wifi_st_dhcp.mask));
	} else if (strcmp(token, "wifi_st_gw")==0) {
		sprintf(buff, IPSTR, IP2STR(&wifi_st_dhcp.gw));
	} else if (strcmp(token, "wifi_st_dhcp")==0) {
		sprintf(buff, "%u", wifi_st_dhcp.mode);
	} else if (strcmp(token, "wifi_st_arec")==0) {
		sprintf(buff, "%u", wifi_st_cfg.autoreconnect);
	} else if (strcmp(token, "wifi_st_rect")==0) {
		sprintf(buff, "%u", wifi_st_cfg.reconnect_pause);
	}

	httpdSend(connData, buff, -1);
	vPortFree(buff);
	return HTTPD_CGI_DONE;
}


