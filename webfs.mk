THISDIR:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))

HOSTCC ?= gcc

#include paths.mk

USE_HEATSHRINK ?= no
GZIP_COMPRESSION ?= no

HTML_PATH ?= html
webpages.espfs: $(HTML_PATH) $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage make_array/make_array
	@mkdir -p $(BIN_DIR)
	cd html; find . | $(THISDIR)$(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage > $(THISDIR)$(BIN_DIR)/webpages.espfs; cd ..	
	echo "char array_webfs[]={" >$(THISDIR)$(BIN_DIR)/array_webfs.c
	make_array/make_array $(THISDIR)$(BIN_DIR)/webpages.espfs >>$(THISDIR)$(BIN_DIR)/array_webfs.c
	echo " };" >>$(THISDIR)$(BIN_DIR)/array_webfs.c
	cd $(THISDIR)

$(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage: $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/
	$(MAKE) -C $(LIBRTLHTTPD_PATH)espfs/mkespfsimage USE_HEATSHRINK="$(USE_HEATSHRINK)" GZIP_COMPRESSION="$(GZIP_COMPRESSION)" all
	
make_array/make_array:
	$(MAKE) -C make_array -f Makefile all 
	
clean_webfs:
	$(MAKE) -C $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/ -f Makefile clean
	$(MAKE) -C make_array -f Makefile clean
	rm -rf $(THISDIR)$(BIN_DIR)/webpages.espfs $(THISDIR)$(BIN_DIR)/array_webfs.c
