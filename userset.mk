#=============================================
# User defined
#=============================================
#SDK_PATH ?= ../../RTL00MP3/RTL00_SDKV35a/
SDK_PATH ?= ../../RTL00_WEB/USDK/
#GCC_PATH = /opt/gcc-arm-none-eabi/bin/
GCC_PATH = /opt/gcc-arm-none-eabi-6-2017-q1-update/bin/
#OPENOCD_PATH = d:/MCU/OpenOCD/bin/# + or set in PATH
TOOLS_PATH ?= ../../RTL8710_SDK_Pick_Padding_Checksum/
#FLASHER_TYPE ?= Jlink
FLASHER_TYPE ?= OCD
FLASHER_PATH ?= ../../rtl8710_openocd/script/
#JLINK_PATH ?= D:/MCU/SEGGER/JLink_V612i/
#JLINK_GDBSRV ?= JLinkGDBServer.exe
LIBRTLHTTPD_PATH = ../../rtlhttpd/librtlhttpd/
