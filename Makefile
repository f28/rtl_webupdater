include userset.mk

all: ram_all
mp: ram_all_mp

.PHONY: ram_all
ram_all: webfs
	@$(MAKE) -j -f $(SDK_PATH)sdkbuild.mk
	@$(MAKE) -f flasher.mk all
	
.PHONY: webfs clean_webfs
webfs:
	@$(MAKE) -f flasher.mk webpages.espfs	
clean_webfs:
	@$(MAKE) -f flasher.mk clean_webfs

.PHONY: ram_all_mp
ram_all_mp:
	@$(MAKE) -j -f $(SDK_PATH)sdkbuild.mk mp
	@$(MAKE) -f flasher.mk mp
	
.PHONY: clean  clean_all
clean: clean_webfs
	@$(MAKE) -j -f $(SDK_PATH)sdkbuild.mk clean
	@$(MAKE) -f flasher.mk clean

clean_all: clean_webfs
	@$(MAKE) -j -f $(SDK_PATH)sdkbuild.mk clean_all
	@$(MAKE) -f flasher.mk clean
	
.PHONY:	debug ramdebug
debug: 
	@$(MAKE) -f application.mk debug	

ramdebug: 
	@$(MAKE) -f application.mk ramdebug	

.PHONY: flashburn runram reset test readfullflash
flashburn: 
	#JLinkGDB-WrFlash.bat
	@$(MAKE) -f flasher.mk flashburn

readfullflash:
	#JLink-RdFullFlash.bat
	@make -f flasher.mk readfullflash 

.PHONY:	prerequirement
prerequirement:
	@$(file >DEPENDENCY_LIST.txt,$(DEPENDENCY_LIST))

#TARGETTYPE := APP
#TARGETNAME := build\obj\build.axf
