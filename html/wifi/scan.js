var RTW_SECURITY ={
	0 :" Open",
	1 :" WEP with open authentication",
	2 :" WEP with shared authentication",
	3 :" WPA with TKIP",
	4 :" WPA with AES",
	5 :" WPA2 with TKIP",
	6 :" WPA2 with AES",
	7 :" WPA2 with AES & TKIP",
	8 :" WPA/WPA2",
	9 :"Unknown"
};

var all_ap=[];

function getSelectedEssid() {
	var e=document.forms.wifiform.elements;
	for (var i=0; i<e.length; i++) {
		if (e[i].type=="radio" && e[i].checked)
			return e[i].value;
	}
	return currAp;
}


function createInputForAp(ap) {
	if (ap.essid=="" && ap.rssi==0) return;
	
	var rssi=document.createElement("td");
	rssi.innerHTML="RSSI:"+ap.rssi+"dBm";
	
	var input=document.createElement("input");
	input.type="radio";
	input.name="essid";
	input.value=ap.essid;
	if (currAp==ap.essid) input.checked="1";
	input.id="opt-"+ap.essid;
	var label=document.createElement("label");
	label.htmlFor="opt-"+ap.essid;
	label.textContent=ap.essid;

	var newrow=document.all.networks.insertRow();
	var newcell=newrow.insertCell(0);
	newcell.appendChild(input);
	newcell=newrow.insertCell(1);
	newcell.appendChild(label);
	
	newcell=newrow.insertCell(2);
	newcell.innerHTML=ap.rssi;
	newcell=newrow.insertCell(3);
	newcell.innerHTML=ap.channel;
	newcell=newrow.insertCell(4);
	if (ap.enc ==-1) {
		newcell.innerHTML="UNKNOWN";
	}
	else {
		newcell.innerHTML=RTW_SECURITY[ap.enc];
	}
}


function connectSelectedEssid() {
	var e=document.forms.wifiform.elements;
	for (var i=0; i<e.length; i++) {
		if (e[i].type=="radio" && e[i].checked) {
			var passw_input = document.getElementsByName('passwd');	
			var body = 'essid=' + encodeURIComponent(all_ap[i].essid) +
  				'&passwd=' + encodeURIComponent(passw_input[0].value) +
  				'&enc=' + encodeURIComponent(all_ap[i].enc) +
				'&wifi_st_ip=' + encodeURIComponent(e.wifi_st_ip.value) +
				'&wifi_ap_msk=' + encodeURIComponent(e.wifi_ap_msk.value) +
				'&wifi_st_gw=' + encodeURIComponent(e.wifi_st_gw.value) +
				'&wifi_st_dhcp=' + encodeURIComponent(e.wifi_st_dhcp.value) +
				'&wifi_st_arec=' + encodeURIComponent(e.wifi_st_arec.value) +
				'&wifi_st_rect=' + encodeURIComponent(e.wifi_st_rect.value);			
			xhr.open("POST", 'connect.cgi', true);
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.onreadystatechange = function() {
				if (xhr.readyState==4) {
					if (xhr.status>=200 && xhr.status<300) {
						alert('OK');
					}
					else {
						alert('Error ' + xhr.status + " : " +xhr.responseText);
					}
				}
			}

			xhr.send(body);

			return e[i].value;
		}
	}
	return currAp;
}

function scanAPs() {
	xhr.open("GET", "wifiscan.cgi");
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status>=200 && xhr.status<300) {
			var data=JSON.parse(xhr.responseText);
			currAp=getSelectedEssid();
			all_ap.length=0;
			if (data.result.inProgress=="0" && data.result.APs.length>1) {
				$("#aps").innerHTML="";
				for (var i=0; i<data.result.APs.length; i++) {
					//if (data.result.APs[i].essid=="" && data.result.APs[i].rssi==0) continue;
					createInputForAp(data.result.APs[i]);
					all_ap.push(data.result.APs[i]);
				}				
				//window.setTimeout(scanAPs, 20000);
			} else {
				window.setTimeout(scanAPs, 1000);				
			}
		}
	}
	xhr.send();
}

window.onload=function(e) {
	scanAPs();
};

